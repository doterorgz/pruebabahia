package com.bahia.pruebatecnica.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.bahia.pruebatecnica.dto.AnimalDetalleDTO;
import com.bahia.pruebatecnica.dto.AnimalListaDTO;
import com.bahia.pruebatecnica.model.service.AnimalService;

@RestController
public class AnimalController {

	@Autowired
	AnimalService animalService;
	
	@GetMapping("/")
	public ResponseEntity<List<AnimalListaDTO>> todos() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(animalService.getAll());
		}
		catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@GetMapping("/animal/{codigo}")
	public ResponseEntity<AnimalDetalleDTO> detalleAnimal(@PathVariable Long codigo) {
		AnimalDetalleDTO result = new AnimalDetalleDTO();
		if (codigo ==null && codigo <=0)  {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		else result = animalService.getDetalleAnimal(codigo);
		if (result == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		else return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	@GetMapping("/animal/especie/{codigo}")
	public ResponseEntity<List<AnimalListaDTO>> listaAnimalesPorEspecie(@PathVariable Long codigo) {
		List<AnimalListaDTO> result = new ArrayList<AnimalListaDTO>();
		if (codigo==null && codigo <=0) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		else {
			result = animalService.getAnimalesPorEspecie(codigo);
		}
		if (result!=null) return ResponseEntity.status(HttpStatus.OK).body(result);
		else return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
}
