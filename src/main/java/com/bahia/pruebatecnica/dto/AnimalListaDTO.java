package com.bahia.pruebatecnica.dto;

import java.io.Serializable;
import java.util.Date;

public class AnimalListaDTO	implements Serializable {
	private long codigo;
	
	private String nombre;
	
	private String  propietario;
	
	private String estado;

	public long getCodigo() {
		return codigo;
	}

	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String propietario) {
		this.propietario = propietario;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
