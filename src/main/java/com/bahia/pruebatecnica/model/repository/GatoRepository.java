package com.bahia.pruebatecnica.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bahia.pruebatecnica.model.entity.Gato;

public interface GatoRepository extends JpaRepository<Gato, Long>{

}
