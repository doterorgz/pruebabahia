package com.bahia.pruebatecnica.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bahia.pruebatecnica.model.entity.Perro;

public interface PerroRepository extends JpaRepository<Perro, Long>{

}
