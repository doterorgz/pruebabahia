package com.bahia.pruebatecnica.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bahia.pruebatecnica.model.entity.Persona;

public interface PersonaRepository extends JpaRepository<Persona, Long>{

}
