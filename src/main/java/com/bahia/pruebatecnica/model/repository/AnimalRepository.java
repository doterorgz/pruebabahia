package com.bahia.pruebatecnica.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bahia.pruebatecnica.model.entity.Animal;

public interface AnimalRepository extends JpaRepository<Animal, Long> {

}
