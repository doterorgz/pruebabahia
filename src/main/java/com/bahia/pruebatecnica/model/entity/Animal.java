package com.bahia.pruebatecnica.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Animal {
	@Id
	@GeneratedValue
	private long codigo;
	
	private String nombre;
	
	private Date fechaNacimiento;
	
	private String estado;
	
	private int numeroVisitas;
	
	@ManyToOne()
	@JoinColumn(name="propietario_id")
	private Persona propietario;


	public long getCodigo() {
		return codigo;
	}


	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public String getEstado() {
		return estado;
	}


	public void setEstado(String estado) {
		this.estado = estado;
	}


	public int getNumeroVisitas() {
		return numeroVisitas;
	}


	public void setNumeroVisitas(int numeroVisitas) {
		this.numeroVisitas = numeroVisitas;
	}


	public Persona getPropietario() {
		return propietario;
	}


	public void setPropietario(Persona propietario) {
		this.propietario = propietario;
	}
	
	public String getDescripcionEspecie() {
		return "Animal";
	}
}
