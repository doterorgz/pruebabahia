package com.bahia.pruebatecnica.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PERRO")
public class Perro extends Animal {
	public String getDescripcionEspecie() {
		return "Canis lupus familiaris";
	}
}
