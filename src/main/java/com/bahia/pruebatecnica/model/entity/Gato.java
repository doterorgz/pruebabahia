package com.bahia.pruebatecnica.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("GATO")
public class Gato extends Animal {

	public String getDescripcionEspecie() {
		return "Felis silvestris catus";
	}
}
