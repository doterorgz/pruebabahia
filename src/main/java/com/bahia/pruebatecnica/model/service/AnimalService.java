package com.bahia.pruebatecnica.model.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bahia.pruebatecnica.dto.AnimalDetalleDTO;
import com.bahia.pruebatecnica.dto.AnimalListaDTO;
import com.bahia.pruebatecnica.model.entity.Animal;
import com.bahia.pruebatecnica.model.repository.AnimalRepository;
import com.bahia.pruebatecnica.model.repository.GatoRepository;
import com.bahia.pruebatecnica.model.repository.PerroRepository;

@Service
public class AnimalService {
	@Autowired
	AnimalRepository animalRepository;
	
	@Autowired
	PerroRepository perroRepository;
	
	@Autowired
	GatoRepository gatoRepository;

	public List<AnimalListaDTO> getAll() {
		List<Animal> result =  animalRepository.findAll();
		List<AnimalListaDTO> animalDto = new ArrayList<AnimalListaDTO>();
		for (Animal animal: result) {
			animalDto.add(animalToAnimalListaDTO(animal)); 
		}
		return animalDto;
	}
	public AnimalDetalleDTO getDetalleAnimal(long idAnimal) {
		Optional<Animal> result =  animalRepository.findById(idAnimal);
		AnimalDetalleDTO animalDto = null;
		if (result.isPresent() ) {
			animalDto = animalToAnimalDTO(result.get()); 
		}
		return animalDto;
	}
	public List<AnimalListaDTO> getAnimalesPorEspecie(String especie) {
		List<AnimalListaDTO> animalDto = new ArrayList<AnimalListaDTO>();
		List<Animal> animales = new ArrayList<Animal>();
		if ("PERRO".equalsIgnoreCase(especie)) {
			animales = (List<Animal>) perroRepository.findAll();
		}
		else {
			animales = gatoRepository.findAll();
		}
		
		for (Animal animal: animales ) {
			animalDto.add(animalToAnimalListaDTO(animal)); 
		}
		return animalDto;
	}
	
	private AnimalDetalleDTO animalToAnimalDTO(Animal animal) {
		AnimalDetalleDTO animalDto = new AnimalDetalleDTO();
		animalDto.setCodigo(animal.getCodigo());
		animalDto.setNombre(animal.getNombre());
		animalDto.setEspecie(animal.getDescripcionEspecie());
		animalDto.setEstado(animal.getEstado());
		animalDto.setNumeroVisitas(animal.getNumeroVisitas());
		animalDto.setPropietario(animal.getPropietario().toString());
		return animalDto;
	}
	private AnimalListaDTO animalToAnimalListaDTO(Animal animal) {
		AnimalListaDTO animalDto = new AnimalListaDTO();
		animalDto.setCodigo(animal.getCodigo());
		animalDto.setNombre(animal.getNombre());
		animalDto.setPropietario(animal.getPropietario().toString());
		animalDto.setEstado(animal.getEstado());
		return animalDto;
	}
}
