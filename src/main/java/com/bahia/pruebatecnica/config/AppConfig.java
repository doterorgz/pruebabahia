package com.bahia.pruebatecnica.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ComponentScan(basePackages = "com.bahia.pruebatecnica")
@Component
public class AppConfig {

}
